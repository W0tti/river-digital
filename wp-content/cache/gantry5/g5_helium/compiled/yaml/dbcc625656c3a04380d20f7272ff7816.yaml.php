<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/Applications/MAMP/htdocs/river-digital/wp-content/themes/g5_helium/custom/config/menu/main-menu.yaml',
    'modified' => 1505999016,
    'data' => [
        'ordering' => [
            'home' => '',
            'features' => [
                'particles' => '',
                'block-variations' => '',
                'typography' => '',
                'documentation' => '',
                'download' => ''
            ],
            'pages' => [
                'blog-page' => '',
                'error-page' => '',
                'offline-page' => ''
            ],
            'layouts' => [
                'full-width' => '',
                'left-sidebar' => '',
                'right-sidebar' => '',
                'two-sidebars' => '',
                'two-sidebars-left' => '',
                'two-sidebars-right' => '',
                'rows-and-columns' => ''
            ],
            'styles' => [
                '__particle-lb4Ja' => ''
            ]
        ],
        'items' => [
            'home' => [
                'object_id' => 126
            ],
            'pages/blog-page' => [
                'object_id' => 128
            ],
            'features' => [
                'object_id' => 3,
                'columns' => [
                    0 => 100
                ]
            ],
            'styles' => [
                'object_id' => 5,
                'dropdown' => 'standard',
                'dropdown_dir' => 'left',
                'dropdown_hide' => '1',
                'width' => '35rem',
                'columns' => [
                    0 => 100
                ]
            ],
            'layouts' => [
                'object_id' => 4
            ],
            'layouts/full-width' => [
                'object_id' => 62
            ],
            'layouts/left-sidebar' => [
                'object_id' => 64
            ],
            'layouts/right-sidebar' => [
                'object_id' => 66
            ],
            'layouts/two-sidebars' => [
                'object_id' => 70
            ],
            'pages' => [
                'object_id' => 6,
                'columns' => [
                    0 => 100
                ]
            ],
            'layouts/two-sidebars-left' => [
                'object_id' => 72
            ],
            'layouts/two-sidebars-right' => [
                'object_id' => 74
            ],
            'layouts/rows-and-columns' => [
                'object_id' => 88
            ],
            'features/block-variations' => [
                'object_id' => 43
            ],
            'features/particles' => [
                'object_id' => 46
            ],
            'pages/offline-page' => [
                'object_id' => 118
            ],
            'features/documentation' => [
                'object_id' => 2
            ],
            'features/download' => [
                'object_id' => 1
            ],
            'pages/error-page' => [
                'object_id' => 7
            ],
            'features/typography' => [
                'object_id' => 9
            ],
            'styles/__particle-lb4Ja' => [
                'type' => 'particle',
                'particle' => 'custom',
                'title' => 'Custom HTML',
                'options' => [
                    'particle' => [
                        'enabled' => '1',
                        'html' => '<div class="g-grid g-preset-thumbnails center">
    <div class="g-block">
        <div class="g-content">
            <a href="?presets=preset1">
                <img src="gantry-media://styles/preset1.png" alt="Preset 1">
                <br />
                <span>Preset 1</span>
            </a>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
            <a href="?presets=preset2">
                <img src="gantry-media://styles/preset2.png" alt="Preset 2">
                <br />
                <span>Preset 2</span>
            </a>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
            <a href="?presets=preset3">
                <img src="gantry-media://styles/preset3.png" alt="Preset 3">
                <br />
                <span>Preset 3</span>
            </a>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
            <a href="?presets=preset4">
                <img src="gantry-media://styles/preset4.png" alt="Preset 4">
                <br />
                <span>Preset 4</span>
            </a>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
            <a href="?presets=preset5">
                <img src="gantry-media://styles/preset5.png" alt="Preset 5">
                <br />
                <span>Preset 5</span>
            </a>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
            <a href="?presets=preset6">
                <img src="gantry-media://styles/preset6.png" alt="Preset 6">
                <br />
                <span>Preset 6</span>
            </a>
        </div>
    </div>    
</div>'
                    ],
                    'block' => [
                        'extra' => [
                            
                        ]
                    ]
                ]
            ]
        ]
    ]
];
