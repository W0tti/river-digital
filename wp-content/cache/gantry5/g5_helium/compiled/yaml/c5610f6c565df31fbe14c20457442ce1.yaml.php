<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/Applications/MAMP/htdocs/river-digital/wp-content/themes/g5_helium/custom/config/default/particles/date.yaml',
    'modified' => 1505999016,
    'data' => [
        'enabled' => '1',
        'css' => [
            'class' => 'date'
        ],
        'date' => [
            'formats' => 'l, F d, Y'
        ]
    ]
];
