<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/Applications/MAMP/htdocs/river-digital/wp-content/themes/g5_helium/custom/config/default/particles/branding.yaml',
    'modified' => 1505999016,
    'data' => [
        'enabled' => '1',
        'content' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry<span class="hidden-tablet"> Framework</span></a>',
        'css' => [
            'class' => 'g-branding'
        ]
    ]
];
