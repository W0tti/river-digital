<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/Applications/MAMP/htdocs/river-digital/wp-content/themes/g5_helium/custom/config/default/particles/contentarray.yaml',
    'modified' => 1505999016,
    'data' => [
        'enabled' => '1',
        'post' => [
            'filter' => [
                'sticky' => ''
            ],
            'limit' => [
                'total' => '2',
                'columns' => '2',
                'start' => '0'
            ],
            'sort' => [
                'orderby' => 'date',
                'ordering' => 'DESC'
            ],
            'display' => [
                'image' => [
                    'enabled' => 'show'
                ],
                'title' => [
                    'enabled' => 'show',
                    'limit' => ''
                ],
                'date' => [
                    'enabled' => 'published',
                    'format' => 'l, F d, Y'
                ],
                'author' => [
                    'enabled' => 'show'
                ],
                'category' => [
                    'enabled' => 'link'
                ],
                'comments' => [
                    'enabled' => 'show'
                ],
                'text' => [
                    'type' => 'content',
                    'limit' => '',
                    'formatting' => 'text'
                ],
                'read_more' => [
                    'enabled' => 'show',
                    'label' => '',
                    'css' => ''
                ]
            ]
        ],
        'css' => [
            'class' => ''
        ],
        'extra' => [
            
        ]
    ]
];
