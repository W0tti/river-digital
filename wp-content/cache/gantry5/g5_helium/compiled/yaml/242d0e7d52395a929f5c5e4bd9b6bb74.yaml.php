<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/Applications/MAMP/htdocs/river-digital/wp-content/themes/g5_helium/custom/config/_body_only/index.yaml',
    'modified' => 1507734667,
    'data' => [
        'name' => '_body_only',
        'timestamp' => 1505999018,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/body-only.png',
            'name' => '_body_only',
            'timestamp' => 1468253830
        ],
        'positions' => [
            
        ],
        'sections' => [
            'mainbar' => 'Mainbar'
        ],
        'particles' => [
            'messages' => [
                'system-messages-6659' => 'System Messages'
            ],
            'content' => [
                'system-content-5845' => 'Page Content'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
