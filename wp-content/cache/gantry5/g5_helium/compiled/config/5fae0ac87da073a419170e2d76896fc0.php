<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1507734684,
    'checksum' => '6a84ef577743a62c448fda0d282008bc',
    'files' => [
        0 => [
            'block_variations' => [
                'file' => 'wp-content/themes/g5_helium/custom/config/block_variations/assignments.yaml',
                'modified' => 1505999014
            ],
            'full_width' => [
                'file' => 'wp-content/themes/g5_helium/custom/config/full_width/assignments.yaml',
                'modified' => 1505999018
            ],
            'home_-_particles' => [
                'file' => 'wp-content/themes/g5_helium/custom/config/home_-_particles/assignments.yaml',
                'modified' => 1505999014
            ],
            'left_sidebar' => [
                'file' => 'wp-content/themes/g5_helium/custom/config/left_sidebar/assignments.yaml',
                'modified' => 1505999014
            ],
            'offline_page' => [
                'file' => 'wp-content/themes/g5_helium/custom/config/offline_page/assignments.yaml',
                'modified' => 1505999014
            ],
            'particles' => [
                'file' => 'wp-content/themes/g5_helium/custom/config/particles/assignments.yaml',
                'modified' => 1505999014
            ],
            'right_sidebar' => [
                'file' => 'wp-content/themes/g5_helium/custom/config/right_sidebar/assignments.yaml',
                'modified' => 1505999016
            ],
            'rows_and_columns' => [
                'file' => 'wp-content/themes/g5_helium/custom/config/rows_and_columns/assignments.yaml',
                'modified' => 1505999014
            ],
            'two_sidebars' => [
                'file' => 'wp-content/themes/g5_helium/custom/config/two_sidebars/assignments.yaml',
                'modified' => 1505999016
            ],
            'two_sidebars_left' => [
                'file' => 'wp-content/themes/g5_helium/custom/config/two_sidebars_left/assignments.yaml',
                'modified' => 1505999014
            ],
            'two_sidebars_right' => [
                'file' => 'wp-content/themes/g5_helium/custom/config/two_sidebars_right/assignments.yaml',
                'modified' => 1505999016
            ]
        ]
    ],
    'data' => [
        'block_variations' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'post' => [
                'page' => [
                    43 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'full_width' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'post' => [
                'page' => [
                    62 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'home_-_particles' => [
            'context' => [
                0 => [
                    'is_front_page' => true
                ]
            ]
        ],
        'left_sidebar' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'post' => [
                'page' => [
                    64 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'offline_page' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'post' => [
                'page' => [
                    118 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'particles' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'post' => [
                'page' => [
                    46 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'right_sidebar' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'post' => [
                'page' => [
                    66 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'rows_and_columns' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'post' => [
                'page' => [
                    88 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'two_sidebars' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'post' => [
                'page' => [
                    70 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'two_sidebars_left' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'post' => [
                'page' => [
                    72 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ],
        'two_sidebars_right' => [
            'context' => [
                
            ],
            'menu' => [
                
            ],
            'post' => [
                'page' => [
                    74 => true
                ]
            ],
            'taxonomy' => [
                
            ],
            'archive' => [
                
            ]
        ]
    ]
];
