<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '59de349d6e4346.77704978',
    'content' => '<div id="custom-5807-particle" class="g-content g-particle">            <div class="fp-intro">
    <h2 class="g-title">Build your beautiful theme, the way you want it, with Helium</h2>
    <p>Craft memorable, emotive experiences with our Gantry 5 framework.</p>
    <div class="ipad-mockup">
        <img src="http://localhost:8888/river-digital/wp-content/themes/g5_helium/images/intro/mockup.png" alt="" />
    </div>
</div>
            </div>'
];
