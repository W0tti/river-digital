<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '59de37467155d5.33985302',
    'content' => '<div id="contentcubes-4979-particle" class="g-content g-particle">            <div class="g-contentcubes ">
    <h2 class="g-title">Content Cubes</h2>
            <div class="cube-items-wrapper">

                            <div class="item image-position-right cube-row g-grid">
                    <div class="g-block size-50">
                                                    <div class="cube-image-wrapper">
                                <img src="http://localhost:8888/river-digital/wp-content/themes/g5_helium/images/above/img02.jpeg" alt="This is a sample of the Content Cubes particle in Helium theme." class="cube-image" />
                            </div>
                                            </div>

                    <div class="g-block size-50">
                        <div class="cube-content-wrapper">
                                                            <div class="item-label">Particle</div>
                            
                            <div class="item-title">
                                                                <a target="_self" class="item-link " href="#">
                                    
                                    This is a sample of the Content Cubes particle in Helium theme.

                                                                        <span class="item-link-text">&rarr;</span>
                                </a>
                                                            </div>

                                                            <div class="item-tags">

                                                                            <span class="tag">
                                            <a target="_self" href="#">
                                                <i class="fa fa-tag"></i>                                                 gantry
                                            </a>
                                        </span>
                                                                            <span class="tag">
                                            <a target="_self" href="#">
                                                <i class="fa fa-tag"></i>                                                 particle
                                            </a>
                                        </span>
                                                                            <span class="tag">
                                            <a target="_self" href="#">
                                                <i class="fa fa-tag"></i>                                                 cube
                                            </a>
                                        </span>
                                    
                                </div>
                                                    </div>
                    </div>
                </div>
                            <div class="item image-position-left cube-row g-grid">
                    <div class="g-block size-50">
                                                    <div class="cube-image-wrapper">
                                <img src="http://localhost:8888/river-digital/wp-content/themes/g5_helium/images/above/img01.jpeg" alt="This is a sample of the Content Cubes particle in Helium theme." class="cube-image" />
                            </div>
                                            </div>

                    <div class="g-block size-50">
                        <div class="cube-content-wrapper">
                                                            <div class="item-label">Particle</div>
                            
                            <div class="item-title">
                                                                <a target="_self" class="item-link " href="#">
                                    
                                    This is a sample of the Content Cubes particle in Helium theme.

                                                                        <span class="item-link-text">&rarr;</span>
                                </a>
                                                            </div>

                                                            <div class="item-tags">

                                                                            <span class="tag">
                                            <a target="_self" href="#">
                                                <i class="fa fa-tag"></i>                                                 grav
                                            </a>
                                        </span>
                                                                            <span class="tag">
                                            <a target="_self" href="#">
                                                <i class="fa fa-tag"></i>                                                 wordpress
                                            </a>
                                        </span>
                                                                            <span class="tag">
                                            <a target="_self" href="#">
                                                <i class="fa fa-tag"></i>                                                 joomla
                                            </a>
                                        </span>
                                    
                                </div>
                                                    </div>
                    </div>
                </div>
            
        </div>
    </div>
            </div>'
];
