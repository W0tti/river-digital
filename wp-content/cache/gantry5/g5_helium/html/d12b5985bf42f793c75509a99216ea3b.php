<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '59de3746726351.98136962',
    'content' => '<div id="custom-2991-particle" class="g-content g-particle">            <p>The <strong>Content Tabs</strong> particle enables you to pack a lot of useful information into a compact space that is easy to configure and intuitive to navigate.</p>

<h3>Particle Configuration</h3>
<div align="center"><p><img src="http://localhost:8888/river-digital/wp-content/uploads/rocketlauncher/pages/particles/content_tabs_1.jpg" /></p></div>
<table>
<thead><tr>
<th align="left">Option</th>
  <th align="left">Description</th>
</tr></thead>
<tbody>
<tr>
<td align="left">CSS Classes</td>
  <td align="left">Sets the CSS class for the content of the particle.</td>
</tr>
<tr>
<td align="left">Animation Type</td>
  <td align="left">Sets the the animation type used when switching between tabs. You can choose between: <strong>Slide Left</strong>, <strong>Slide Right</strong>, <strong>Slide Up</strong>, <strong>Slide Down</strong>, <strong>Fade</strong>, and <strong>Toggle</strong>. </td>
</tr>
</tbody>
</table>

<h3>Item Configuration</h3>
<div align="center"><p><img src="http://localhost:8888/river-digital/wp-content/uploads/rocketlauncher/pages/particles/content_tabs_2.jpg" /></p></div>
<table>
<thead><tr>
<th align="left">Option</th>
  <th align="left">Description</th>
</tr></thead>
<tbody>
<tr>
<td align="left">Item Name</td>
  <td align="left">The name of the item becomes the title used for the tab on the front end.</td>
</tr>
<tr>
<td align="left">Tab Content</td>
  <td align="left">Enter HTML content you would like to display in your tab as its content body here.</td>
</tr>
</tbody>
</table>
            </div>'
];
