<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '59de373ff07573.95792128',
    'content' => '<div id="copyright-1736-particle" class="g-content g-particle">            <div class="g-copyright ">
    &copy;
            Gantry Framework
        2016 -     2017
    <br />Developed by RocketTheme exclusively<br />for Gantry 5.</div>
            </div>'
];
