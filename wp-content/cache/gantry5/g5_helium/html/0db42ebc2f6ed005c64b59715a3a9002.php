<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '59de37467094a1.34852775',
    'content' => '<div id="owlcarousel-3850-particle" class="g-content g-particle">            <div class="">
        <h2 class="g-title">OwlCarousel</h2>
        <div id="g-owlcarousel-owlcarousel-3850" class="g-owlcarousel owl-carousel has-color-overlay">

                            <div class="g-owlcarousel-item owl-item">
                    <div class="g-owlcarousel-item-wrapper">
                        <div class="g-owlcarousel-item-img">
                            <img src="http://localhost:8888/river-digital/wp-content/themes/g5_helium/images/header/img02.jpg" alt="Owl Carousel" />
                        </div>
                        <div class="g-owlcarousel-item-content-container">
                            <div class="g-owlcarousel-item-content-wrapper">
                                <div class="g-owlcarousel-item-content">
                                                                            <h1 class="g-owlcarousel-item-title">Owl Carousel</h1>                                                                            <h2 class="g-owlcarousel-item-desc">The Owl Carousel particle features premium functionality from the start.</h2>                                                                            <div class="g-owlcarousel-item-link">
                                            <a target="_self" class="g-owlcarousel-item-button button button-2" href="#">
                                                Click Me
                                            </a>
                                        </div>
                                                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                            <div class="g-owlcarousel-item owl-item">
                    <div class="g-owlcarousel-item-wrapper">
                        <div class="g-owlcarousel-item-img">
                            <img src="http://localhost:8888/river-digital/wp-content/themes/g5_helium/images/header/img01.jpg" alt="Owl Carousel" />
                        </div>
                        <div class="g-owlcarousel-item-content-container">
                            <div class="g-owlcarousel-item-content-wrapper">
                                <div class="g-owlcarousel-item-content">
                                                                            <h1 class="g-owlcarousel-item-title">Owl Carousel</h1>                                                                            <h2 class="g-owlcarousel-item-desc">Set up and deploy a gorgeous carousel in seconds that visitors will love.</h2>                                                                            <div class="g-owlcarousel-item-link">
                                            <a target="_self" class="g-owlcarousel-item-button button button-2" href="#">
                                                Click Me
                                            </a>
                                        </div>
                                                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </div>
    </div>
            </div>',
    'frameworks' => [
        'jquery' => 1
    ],
    'scripts' => [
        'footer' => [
            'e7f0cbb1371d843d2d18026cd34cde1de5ce9947b39011fc9c580147cf1e473b955f03d8' => [
                ':type' => 'file',
                ':priority' => 0,
                'src' => 'http://localhost:8888/river-digital/wp-content/themes/g5_helium/js/owlcarousel.js',
                'type' => 'text/javascript',
                'defer' => false,
                'async' => false,
                'handle' => ''
            ],
            '1930712951b89dd759e176371f68400f7b5bf2d271de8b0338fdc84d293bf875e00d8888' => [
                ':type' => 'inline',
                ':priority' => 0,
                'content' => '
        jQuery(window).load(function() {
            jQuery(\'#g-owlcarousel-owlcarousel-3850\').owlCarousel({
                items: 1,
                rtl: false,
                loop: true,
                                nav: true,
                navText: [\'\\x3Ci\\x20class\\x3D\\x22fa\\x20fa\\x2Dchevron\\x2Dleft\\x22\\x20aria\\x2Dhidden\\x3D\\x22true\\x22\\x3E\\x3C\\x2Fi\\x3E\', \'\\x3Ci\\x20class\\x3D\\x22fa\\x20fa\\x2Dchevron\\x2Dright\\x22\\x20aria\\x2Dhidden\\x3D\\x22true\\x22\\x3E\\x3C\\x2Fi\\x3E\'],
                                                dots: false,
                                                autoplay: false,
                            })
        });
    ',
                'type' => 'text/javascript'
            ]
        ]
    ]
];
