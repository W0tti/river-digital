<?php

/* @gantry-admin/partials/php_unsupported.html.twig */
class __TwigTemplate_d29def7ef6d64c1d2d2f820a3f84ceec95e4d826917f9d3eacc6681d9f9a399d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["php_version"] = twig_constant("PHP_VERSION");
        // line 2
        echo "
";
        // line 3
        if ((is_string($__internal_7eda657fd6037699cac3aed5712216da31212c18aef1babdb4968b629a2ebc55 = ($context["php_version"] ?? null)) && is_string($__internal_9a3ad3fa23678aaabc8923b422d8ee178605f621729161920a155203aee60e19 = "5.4") && ('' === $__internal_9a3ad3fa23678aaabc8923b422d8ee178605f621729161920a155203aee60e19 || 0 === strpos($__internal_7eda657fd6037699cac3aed5712216da31212c18aef1babdb4968b629a2ebc55, $__internal_9a3ad3fa23678aaabc8923b422d8ee178605f621729161920a155203aee60e19)))) {
            // line 4
            echo "<div class=\"g-grid\">
    <div class=\"g-block alert alert-warning g-php-outdated\">
        ";
            // line 6
            echo $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->transFilter("GANTRY5_PLATFORM_PHP54_WARNING", ($context["php_version"] ?? null));
            echo "
    </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "@gantry-admin/partials/php_unsupported.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 6,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@gantry-admin/partials/php_unsupported.html.twig", "/Applications/MAMP/htdocs/river-digital/wp-content/plugins/gantry5/admin/templates/partials/php_unsupported.html.twig");
    }
}
