<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_riverdigital');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'v.jRh-Be$8 bs8;yT[UnF06^HPUZ2j}Au|l4HexTQm]H:#ku&XRZ]+e6dL8:Gmfu');
define('SECURE_AUTH_KEY',  'SDQ?L+ioF]QfY(QF9{o43R Jmt<!KNjW1xgBAytYUn |$0AMctuQn`TnpN5B,k0<');
define('LOGGED_IN_KEY',    '_WK(vVnT6|pWKBAJ^xdoJ$B`Y>PdV#uqQbc16<y5+a(I6>PL69PrD={;kn;}PHY<');
define('NONCE_KEY',        '*t6`8sRJ:YeDiVXZ+43;;X~PSidlTV4kw0<9EC2nL(i~weLrf`%aC ]b,s~xXwN&');
define('AUTH_SALT',        'r3!8=$OX`21jj~Njc/>$}E;q>6,jIPAds)pL}b66uH {jjC`}WYqAGj)K>Ts2?gO');
define('SECURE_AUTH_SALT', '&l%RP/:(I!u?&)Y8;F#t9,U2RWM~H$VRjCN|p~ja-S]l}}8pl4HHX{5imuIN}QJe');
define('LOGGED_IN_SALT',   'Q=Q/Y5h-WI]l,Rtwg&T&jR?UnPzcDWfON&$25]g)?^^(mf%? F>.^mWPD4o;QGbu');
define('NONCE_SALT',       'T&~|l/ciF.E5=F^`3[J%s)TtK60}5D?Cb&np098w5ir^6`c6,7F<Chm%]u9OEEpP');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
